const express = require('express');
const app = express();
const port = 8002;
const goodsRouter = require('./routes/goods');

app.get('/', (req, res) => {
  res.send('Hello, World!');
});

// If '/api' added on PATH, use goodsRouter.
app.use('/api', [goodsRouter]);

app.listen(port, () => {
  console.log(`${port}로 web server가 열렸어요.`);
});
