const express = require('express');
const router = express.Router();

// localhost:8002/api/ GET
router.get('/', (req, res) => {
  res.send('default url for goods.js GET Method.');
});

// localhost:8002/api/about GET
router.get('/about', (req, res) => {
  res.send('goods.js about PATH');
});

module.exports = router;
